FROM golang:1.12-alpine AS go_base

RUN apk add --no-cache git

RUN go get github.com/cybaspace/sap-commerce-manifest-tool

FROM jenkins/agent

COPY --from=go_base /go/bin/sap-commerce-manifest-tool /usr/local/bin/sap-commerce-manifest-tool

